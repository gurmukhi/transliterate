Transliterate Gurmukhi  
======================  
Transliterate Gurmukhi to/from ATB New System.
This mapping is compatible with the ascii TrueType font family known as "GurbaniAkhar".  
  
Linux / OSX Usage:  
----------  
Encode Gurmukhi ascii characters to ATB New System utf-8 encoding:  
``` shell
./gurmukhi-transliterate.py -e "TC"

-> ṭhch̄
```  
  
or  
  
``` shell
echo "-e TC" | ./gurmukhi-transliterate.py

-> ṭhch̄
```  
  
----------  
  
Decode ATB New System utf-8 characters to Gurmukhi ascii encoding:  
  
``` shell
./gurmukhi-transliterate.py -d "ṭhch̄"

-> TC
```  
  
or  
  
``` shell
echo "-d ṭhch̄" | ./gurmukhi-transliterate.py

-> TC
```  
  
----------  
  
Full documentation:  
  
``` shell
$ ./gurmukhi-transliterate.py
gurmukhi-transliterate.py v0.1.0

usage: gurmukhi-transliterate.py [-h] [-e Gurmukhi ascii characters to encode]
                                 [-d ATB New System utf-8 characters to decode]
                                 [-g] [-a] [-j] [-v] [-vb [0-10]]

Transliterate Gurmukhi to/from ATB New System.

optional arguments:
  -h, --help            show this help message and exit
  -e Gurmukhi ascii character to encode, --encode Gurmukhi ascii character to encode
                        Transliterate Gurmukhi to ATB New System (expects
                        Gurmukhi ascii encoded characters as input).
  -d ATB New System utf-8 character to decode, --decode ATB New System utf-8 character to decode
                        Transliterate ATB New System to Gurmukhi (expects ATB
                        New System utf-8 encoded characters as input).
  -g, --gurmukhi        View the currently enabled Gurmukhi to ATB New System
                        encoding rules. Note that this arg refers to the input
                        character’s system, not the system you want to
                        transliterate into.
  -a, --atb-new-system  View the currently enabled ATB New System to Gurmukhi
                        decoding rules. Note that this arg refers to the input
                        character’s system, not the system you want to
                        transliterate into.
  -j, --json            Output in JSON format. Intended to be combined with
                        some other flags, such as -g and -a. It cannot be used
                        on its own.
  -v, --version         Print the current version number of this software to
                        stdout.
  -vb [0-10], --verbose [0-10]
                        Set verbosity level for log and error messages.
```
  
  
==================  
  
Windows Usage:  
----------    
Windows is not currently supported unfortunately because I couldn't get
Unicode working properly in Powershell or CMD. It almost worked though.
  
======================  
