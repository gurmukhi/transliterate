#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Transliterate Gurmukhi to/from ATB New System.
# This mapping is compatible with the ASCII
# TrueType font family known as "GurbaniAkhar".
import sys
import os
import argparse
import pprint
import json

# This version number is reported when you run the program 
# with the -v flag.
version = "0.1.0"

# Logging verbosity level: 0-10
verbose = 0


# ----- Special encoding/decoding rule functions -----

# Lookup
def lookup(in_char, in_orig, in_type, in_phrase, in_i, prev_val, code_chars):
    if in_char in code_chars:
        return code_chars[in_char][0]
    elif in_orig in code_chars:
        return code_chars[in_orig][0]
    else:
        return in_char


# Add an 'a' after consonant.
def add_a_after_consonant(in_char, in_orig, in_type, in_phrase, in_i, prev_val, code_chars):
    if in_type is 'con':
        return in_char + 'a'
    
    return in_char

# Remove last 'a' character.
def remove_last_a(in_char, in_orig, in_type, in_phrase, in_i, prev_val, code_chars):
    if (len(in_char) > 1) and (in_char[-1] == 'a'):
        return ''.join(in_char[0:-1])
    
    return in_char

# Add an 'a' character to the end.
def add_last_a(in_char, in_orig, in_type, in_phrase, in_i, prev_val, code_chars):
    return in_char + 'a'

def lower_r_if_after_vowel_or_beginning_word(in_char, in_orig, in_type, in_phrase, in_i, prev_val, code_chars):
    if (in_orig is 'r'):
        if (in_i == 0) or (len(prev_val) < 2):
            return in_orig

        if (in_i > 0) and (len(prev_val) > 1):
            prev_char_type = prev_val[-2]

            # If after vowel or beginning of word.
            if (prev_char_type is 'vow') or (prev_val[0] is ' '):
                return in_orig

    return in_char

# ----- End Special encoding/decoding rule functions -----


# Encoding table: Mapping for Gurmukhi => ATB New System.
encode_chars = {
    # Consonants - get an automatic 'a' after them by default
    'B': ['bh', 'con', [lookup, add_a_after_consonant]],
    'C': ['ch̄', 'con', [lookup, add_a_after_consonant]],
    'D': ['dh', 'con', [lookup, add_a_after_consonant]],
    'F': ['ḍh', 'con', [lookup, add_a_after_consonant]],
    'G': ['gh', 'con', [lookup, add_a_after_consonant]],
    'J': ['jh', 'con', [lookup, add_a_after_consonant]],
    'K': ['kh', 'con', [lookup, add_a_after_consonant]],	
    'L': ['l̨\'', 'con', [lookup, add_a_after_consonant]],
    'P': ['ph', 'con', [lookup, add_a_after_consonant]],
    'Q': ['th', 'con', [lookup, add_a_after_consonant]],
    'S': ['sh', 'con', [lookup, add_a_after_consonant]],	
    'T': ['ṭh', 'con', [lookup, add_a_after_consonant]],
    'V': ['ṛ', 'con', [lookup, add_a_after_consonant]],
    'X': ['y', 'con', [lookup, add_a_after_consonant]],
    'Z': ['g̨\'', 'con', [lookup, add_a_after_consonant]],
    '\\': ['ñ', 'con', [lookup, add_a_after_consonant]],
    '^': ['k̨h\'', 'con', [lookup, add_a_after_consonant]],
    'b': ['b', 'con', [lookup, add_a_after_consonant]],
    'c': ['ch', 'con', [lookup, add_a_after_consonant]],
    'd': ['d', 'con', [lookup, add_a_after_consonant]],
    'f': ['ḍ', 'con', [lookup, add_a_after_consonant]],
    'g': ['g', 'con', [lookup, add_a_after_consonant]], 
    'h': ['h', 'con', [lookup, add_a_after_consonant]],
    'j': ['j', 'con', [lookup, add_a_after_consonant]],
    'k': ['k', 'con', [lookup, add_a_after_consonant]],
    'l': ['l', 'con', [lookup, add_a_after_consonant]],
    'm': ['m', 'con', [lookup, add_a_after_consonant]],
    'n': ['n', 'con', [lookup, add_a_after_consonant]],
    'p': ['p', 'con', [lookup, add_a_after_consonant]],
    'q': ['t', 'con', [lookup, add_a_after_consonant]],
    'r': ['R', 'con', [lookup, lower_r_if_after_vowel_or_beginning_word, add_a_after_consonant]],    # When after a vowel or at the beginning of a word it should map to 'r' instead.
    's': ['s', 'con', [lookup, add_a_after_consonant]],
    't': ['ṭ', 'con', [lookup, add_a_after_consonant]],
    'v': ['ʋ', 'con', [lookup, add_a_after_consonant]],
    'x': ['ṇ', 'con', [lookup, add_a_after_consonant]],
    'z': ['z', 'con', [lookup, add_a_after_consonant]],
    '|': ['ŋ', 'con', [lookup, add_a_after_consonant]],
    '&': ['f', 'con', [lookup, add_a_after_consonant]],

    # Vowels
    'A': ['a', 'vow', [lookup]],

    # Add-ons
    ' ': [' ', 'add', [lookup]],   # Allow spaces.
    '@': ['`', 'add', [lookup, remove_last_a]],    # Add back vowel that was removed after consonant.

    # TODO: Not sorted yet
    '': 'a',
    'w': 'ā',
    'i': 'i',
    'I': 'ī',
    'u': 'u',
    'U': 'ū',
    'y': 'e',
    'Y': 'æ',
    'E': 'o',
    'O': 'õ',
    'M': 'n̊',
    'N': 'ṅ',
    'W': 'āṅ',
    'R': 'r',   # NOTE: Should this also map to uppercase R? Added it for now.
    'H': 'ḩ',
    # '`': '`',   # Adds a line above the consonant after it.
}

# Decoding table: Mapping for ATB New System => Gurmukhi.
decode_chars = {
    's': 's', 
    'h': 'h',
    'k': 'k', 
    'kh': 'K', 	
    'g': 'g',  
    'gh': 'G', 
    'ŋ': '|', 
    'ch': 'c', 
    'ch̄': 'C', 	
    'j': 'j',
    'jh': 'J', 
    'ñ': '\\',	
    'ṭ': 't', 
    'ṭh': 'T', 
    'ḍ': 'f',
    'ḍh': 'F', 
    'ṇ': 'x', 
    't': 'q', 
    'th': 'Q', 
    'd': 'd', 
    'dh': 'D', 
    'n': 'n', 
    'p': 'p', 
    'ph': 'P', 
    'b': 'b', 
    'bh': 'B', 
    'm': 'm', 
    'y': 'X', 
    # 'r': 'r',         # NOTE: Duplicate, see the other 'r' key below.
    'R': 'r',    # NOTE: Added this rule to fix the grammar logic, not sure if it's appropriate though.
    'l': 'l', 
    'v': 'v', 
    'ṛ': 'V', 
    'sh': 'S', 	
    'z': 'z', 
    'g̨': 'Z', 
    'k̨h': '^', 	
    'f': '&', 
    'l̨': 'L', 
    'a': '', 
    'ā': 'w', 
    'i': 'i', 
    'ī': 'I', 
    'u': 'u', 
    'ū': 'U',
    'e': 'y', 
    'æ': 'Y', 
    'o': 'E',
    'õ': 'O', 
    'n̊': 'M', 
    'ṅ': 'N',
    'āṅ': 'W', 
    'r': 'R',    # NOTE: Is this mapping correct?
    'ḩ': 'H', 
    # '`': '`',   # Adds a line above the consonant after it.
    ' ': ' ',   # Allow spaces.
    '`': ['@', [add_last_a]],  # First special decoding rule.
}

# Calculate the maximum number of characters per character that
# the language ruleset contains. This is needed to tokenize phrases. 
# For example, the character 'k̨h' is actually two characters,
# and there could potentially be some which are composed of even 
# more characters, so it's best not to make any assumptions about
# how many characters a character could be composed of.
def max_char_width(code_chars):
    max_width = 0

    for key in code_chars:
        width = len(key)

        if width > max_width:
            max_width = width
    
    return max_width

# Transliterate a phrase using a dict mapping 
# passed in as the 'code_chars' parameter.
# This is the function which does the actual
# transliteration.
def transliterate(phrase, code_chars):
    # A list which we will add the transliterated tokens to
    # before joining them together, turning it into a string,
    # and returning it so we can output the result of the
    # transliteration later to stdout.
    res = []

    # Calculate and save the maximum character width
    # supported by our current ruleset.
    current_char_width = max_char_width(code_chars)

    # Copy the maximum character width to another
    # variable which we will modify as we iterate
    # over the phrase to keep track of the tokenizing
    # logic.
    last_char_width = current_char_width

    # A counter to increment as we move through the
    # characters in the input phrase, so we know
    # when we're done reading the whole phrase.
    i = 0

    # Loop until we have reached the end of the phrase.
    while i < len(phrase):
        # Save the next character from the input phrase into
        # a variable to use later.
        current_char = phrase[i:i+last_char_width]
        
        last_char = phrase[i-last_char_width:i]

        # If that character is found in our current ruleset.
        if current_char in code_chars:
            # Save the current character into a variable.
            val = code_chars[current_char]

            # If it's a regular rule.
            if isinstance(val, (bytes, str)):

                # Transliterate it and add the result to our result list.
                res.append(val)

                # Increment the 'i' variable enough times so we get to
                # the next character in the input phrase.
                i += last_char_width

                # Reset the width of character we are looking for to the
                # maximum character width supported by our current ruleset.
                last_char_width = current_char_width

                # Skip the rest of this loop iteration and continue onto the 
                # next iteration.
                continue

            # If it's a special rule requiring some special behaviour.
            elif type(val) is list:
                char = current_char
                char_type = val[-2]
                spec_funcs = val[-1]                
                out_val = current_char
                # last_val = ''

                # if i > last_char_width:
                last_val = code_chars[last_char]

                if len(spec_funcs) > 0:
                    for spec_func in spec_funcs:
                        out_val = spec_func(out_val, char, char_type, phrase, i, last_val, code_chars)

                res.append(out_val)
                # res.append(code_chars[out_val][0])

                # Increment the 'i' variable enough times so we get to
                # the next character in the input phrase.
                i += last_char_width

                # Reset the width of character we are looking for to the
                # maximum character width supported by our current ruleset.
                last_char_width = current_char_width

                # Skip the rest of this loop iteration and continue onto the 
                # next iteration.
                continue

        # If we have tried all character widths starting at our maximum,
        # and reducing by 1 each time until we get to width 0.
        if last_char_width < 1:
            # If the character is not found in our current ruleset.
            if current_char not in code_chars:
                # Add a question mark surrounded by square brackets placeholder to our result list.
                res.append("[?]")

            # Increment the 'i' variable enough times so we get to
            # the next character in the input phrase.
            i += (last_char_width + 1)

            # Reset the width of character we are looking for to the
            # maximum character width supported by our current ruleset.
            last_char_width = current_char_width

            # Skip the rest of this loop iteration and continue onto the 
            # next iteration.
            continue

        # Decrement the variable which controls which width of character 
        # we are currently looking for in our ruleset.
        last_char_width -= 1

    # When we reach this line, we are done reading through the input phrase,
    # we have transliterated everything, saved it into the 'res' variable,
    # which is a list, so here we convert the list into a string and return
    # it so another part of the program can output this final result to 
    # stdout.
    return ''.join(res)

# Initialize the program.
def init():
    # Commandline arguments are specified here.
    parser = argparse.ArgumentParser(
        description='Transliterate Gurmukhi to/from ATB New System.')

    # Transliterate Gurmukhi to ATB New System.
    parser.add_argument('-e', '--encode', metavar='Gurmukhi ascii characters to encode', dest='encode',
        default=argparse.SUPPRESS,
        nargs=1,
        help='Transliterate Gurmukhi to ATB New System (expects Gurmukhi ascii encoded characters as input).')

    # Transliterate ATB New System to Gurmukhi.
    parser.add_argument('-d', '--decode', metavar='ATB New System utf-8 characters to decode', dest='decode',
        default=argparse.SUPPRESS,
        nargs=1,
        help='Transliterate ATB New System to Gurmukhi (expects ATB New System utf-8 encoded characters as input).')

    # Print the encoding rules to stdout.
    parser.add_argument('-g', '--gurmukhi', dest='gurmukhi',
        default=argparse.SUPPRESS,
        action='store_true',
        help='View the currently enabled Gurmukhi to ATB New System encoding rules. Note that this arg refers to the input character\'s system, not the system you want to transliterate into.')

    # Print the decoding rules to stdout.
    parser.add_argument('-a', '--atb-new-system', dest='atb',
        default=argparse.SUPPRESS,
        action='store_true',
        help='View the currently enabled ATB New System to Gurmukhi decoding rules. Note that this arg refers to the input character\'s system, not the system you want to transliterate into.')

    # Print the output in JSON format.
    parser.add_argument('-j', '--json', dest='json',
        default=argparse.SUPPRESS,
        action='store_true',
        help='Output in JSON format. Intended to be combined with some other flags, such as -g and -a. It cannot be used on its own.')

    # Print the program version to stdout.
    parser.add_argument('-v', '--version', dest='version',
        default=argparse.SUPPRESS,
        action='store_true',
        help='Print the current version number of this software to stdout.')

    # Set the verbosity level for log messages and output.
    parser.add_argument('-vb', '--verbose', metavar='0-10', dest='verbose',
        type=int,
        default=0,
        nargs='?',
        help='Set verbosity level for log and error messages.')

    # A list of any arguments the script has been invoked with.
    args, leftovers = parser.parse_known_args()

    # A list to hold user input coming from stdin.
    inp = []

    # Save user input into the 'inp' variable.
    if not sys.stdin.isatty():  # If the user sent their input to be transliterated on stdin,
        for line in sys.stdin:
            inp.append(line.rstrip('\n').rstrip(' '))   # add the input to the 'inp' variable.

        # Get the first line of input and split it on spaces into a list.
        inp = inp[0].split(' ')

        # Parse the commandline arguments from stdin.
        args, leftovers = parser.parse_known_args(inp)

    return args, leftovers, parser

def to_json(d):
    res = {}

    # for k in enumerate(d):
    #     if k in d:
    #         res[k] = d[k]
    #         if isinstance(d[k][1], (bytes, str)) is not True:
    #             if len(d[k][1]) > 2:
    #                 fns = d[k][1][2]
    #                 res[k][1][2] = []

    #                 for j in enumerate(fns):
    #                     if callable(fns[j]):
    #                         res[k][2].append(fns[j].__name__)
    #                         continue

    return res

# Run the program.
def run(args, leftovers, parser):
    global verbose

    # The pp variable can be used to print dicts with spacing, and other.
    pp = pprint.PrettyPrinter(indent=4)

    # Set verbosity level for logging and error messages.
    # Defaults to 0, and if you pass -vb with no number after it,
    # it defaults to 1 in that case.
    if 'verbose' in args:
        if type(args.verbose) is list:
            if len(args.verbose) > 0:
                verbose = args.verbose[0]
        else:
            verbose = args.verbose
        
        if verbose == None:
            verbose = 1

    # Print program name and version to stdout.
    if 'version' in args:
        print(os.path.basename(sys.argv[0]) + ' v' + version)
        # return True

    # Print Gurmukhi to ATB New System encoding rules to stdout.
    if 'gurmukhi' in args:
        # Print in JSON format if the commandline arg -j used along with -g.
        if 'json' in args:
            print(json.dumps(to_json(encode_chars), indent=4, sort_keys=True))
            return True

        # Print in standard format which respects UTF-8 encoding.
        pp.pprint(encode_chars)
        return True

    # Print ATB New System to Gurmukhi decoding rules to stdout.
    if 'atb' in args:
        # Print in JSON format if the commandline arg -j used along with -a.
        if 'json' in args:
            print(json.dumps(to_json(decode_chars), indent=4, sort_keys=True))
            return True

        # Print in standard format which respects UTF-8 encoding.
        pp.pprint(decode_chars)
        return True
    
    # Encode or decode the character, depending on which commandline 
    # arguments the user invokes this program with, then print the
    # result to stdout.
    if 'decode' in args:
        print(transliterate(''.join(args.decode), decode_chars))

    elif 'encode' in args:
        print(transliterate(''.join(args.encode), encode_chars))

    # If no arguments are specified, print version and usage, and exit with an error code.
    else:
        print(os.path.basename(sys.argv[0]) + ' v' + version + '\n')
        parser.print_help()
        print()
        return 1
    
    # Indicate that we were successful in transliterating if we reach 
    # this point in the program by returning True.
    return True

# Main entrypoint.
def main():
    # Initialize the program.
    args, leftovers, parser = init()

    # Run the program.
    res = run(args, leftovers, parser)

    return res

# Run main() and save the return value in a variable called 'exitcode'.
exitcode = main()

# Check exitcode and handle any errors here.
if exitcode is not True:
    if verbose > 0:
        print('error: main() returned error exitcode: %d' % exitcode)

    sys.exit(exitcode)

# Exit with the success exit code, so any shell program invoking this can verifiy if execution was successful.
sys.exit(0)
